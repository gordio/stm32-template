#include <stm32f10x.h>
/*#include <stm32f10x_gpio.h>*/
/*#include <stm32f10x_rcc.h>*/

#define LED_PORT GPIOC
#define LED_BLUE (1 << 8)
#define LED_GREEN (1 << 9)

int main(int argc, char const *argv[])
{
	/* setup i/o port */
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
	LED_PORT->CRH |= GPIO_CRH_MODE8_0 | GPIO_CRH_MODE9_0;
	LED_PORT->CRH &= ~(GPIO_CRH_CNF8 | GPIO_CRH_CNF9);
	/* enable led */
	LED_PORT->ODR = LED_GREEN;
	/* infinity loop */
	while (1);

	return 0;
}
